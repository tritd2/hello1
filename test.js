const metaValueProductStdVariant = [
    {
        "meta_values": [
            {
                "text": "Black",
                "code": "black",
                "type": "COLORS",
                "sku_code": "BLK",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1aab" }
            },
            {
                "text": "XS",
                "code": "xs",
                "type": "SIZES",
                "sku_code": "AA00",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1aac" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Black",
                "code": "black",
                "type": "COLORS",
                "sku_code": "BLK",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ab1" }
            },
            {
                "text": "S",
                "code": "s",
                "type": "SIZES",
                "sku_code": "AA01",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ab2" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Black",
                "code": "black",
                "type": "COLORS",
                "sku_code": "BLK",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ab7" }
            },
            {
                "text": "M",
                "code": "m",
                "type": "SIZES",
                "sku_code": "AA02",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ab8" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Black",
                "code": "black",
                "type": "COLORS",
                "sku_code": "BLK",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1abd" }
            },
            {
                "text": "L",
                "code": "l",
                "type": "SIZES",
                "sku_code": "AA03",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1abe" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Black",
                "code": "black",
                "type": "COLORS",
                "sku_code": "BLK",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ac3" }
            },
            {
                "text": "XL",
                "code": "xl",
                "type": "SIZES",
                "sku_code": "AA04",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ac4" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Black",
                "code": "black",
                "type": "COLORS",
                "sku_code": "BLK",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ac9" }
            },
            {
                "text": "2XL",
                "code": "2xl",
                "type": "SIZES",
                "sku_code": "AA05",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1aca" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Black",
                "code": "black",
                "type": "COLORS",
                "sku_code": "BLK",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1acf" }
            },
            {
                "text": "3XL",
                "code": "3xl",
                "type": "SIZES",
                "sku_code": "AA06",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ad0" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Brown",
                "code": "brown",
                "type": "COLORS",
                "sku_code": "BRO",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ad5" }
            },
            {
                "text": "XS",
                "code": "xs",
                "type": "SIZES",
                "sku_code": "AA00",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ad6" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Brown",
                "code": "brown",
                "type": "COLORS",
                "sku_code": "BRO",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1adb" }
            },
            {
                "text": "S",
                "code": "s",
                "type": "SIZES",
                "sku_code": "AA01",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1adc" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Brown",
                "code": "brown",
                "type": "COLORS",
                "sku_code": "BRO",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ae1" }
            },
            {
                "text": "M",
                "code": "m",
                "type": "SIZES",
                "sku_code": "AA02",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ae2" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Brown",
                "code": "brown",
                "type": "COLORS",
                "sku_code": "BRO",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ae7" }
            },
            {
                "text": "L",
                "code": "l",
                "type": "SIZES",
                "sku_code": "AA03",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1ae8" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Brown",
                "code": "brown",
                "type": "COLORS",
                "sku_code": "BRO",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1aed" }
            },
            {
                "text": "XL",
                "code": "xl",
                "type": "SIZES",
                "sku_code": "AA04",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1aee" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Brown",
                "code": "brown",
                "type": "COLORS",
                "sku_code": "BRO",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1af3" }
            },
            {
                "text": "2XL",
                "code": "2xl",
                "type": "SIZES",
                "sku_code": "AA05",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1af4" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Brown",
                "code": "brown",
                "type": "COLORS",
                "sku_code": "BRO",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1af9" }
            },
            {
                "text": "3XL",
                "code": "3xl",
                "type": "SIZES",
                "sku_code": "AA06",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1afa" }
            }
        ]
    },
    {
        "_id": { "$oid": "6594c2a068ff9162623e9f52" },
        "meta_values": [
            {
                "text": "Dark Heather",
                "code": "darkheather",
                "type": "COLORS",
                "sku_code": "DKH",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1aff" }
            },
            {
                "text": "XS",
                "code": "xs",
                "type": "SIZES",
                "sku_code": "AA00",
                "_id": { "$oid": "65a628297e3cfdbf8c0d1b00" }
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Dark Heather",
                "code": "darkheather",
                "type": "COLORS",
                "sku_code": "DKH",
            },
            {
                "text": "S",
                "code": "s",
                "type": "SIZES",
                "sku_code": "AA01",
            }
        ]
    },
    {
        "_id": { "$oid": "6594c2a068ff9162623e9f62" },
        "meta_values": [
            {
                "text": "Dark Heather",
                "code": "darkheather",
                "type": "COLORS",
                "sku_code": "DKH",
            },
            {
                "text": "M",
                "code": "m",
                "type": "SIZES",
                "sku_code": "AA02"
            }
        ]
    },
    {
        "_id": { "$oid": "6594c2a068ff9162623e9f6a" },
        "meta_values": [
            {
                "text": "Dark Heather",
                "code": "darkheather",
                "type": "COLORS",
                "sku_code": "DKH",
            },
            {
                "text": "L",
                "code": "l",
                "type": "SIZES",
                "sku_code": "AA03",
            }
        ]
    },
    {
        "meta_values": [
            {
                "text": "Dark Heather",
                "code": "darkheather",
                "type": "COLORS",
                "sku_code": "DKH",
            },
            {
                "text": "XL",
                "code": "xl",
                "type": "SIZES",
                "sku_code": "AA04",
            }
        ]
    },
    {
        "_id": { "$oid": "6594c2a068ff9162623e9f7a" },
        "meta_values": [
            {
                "text": "Dark Heather",
                "code": "darkheather",
                "type": "COLORS",
                "sku_code": "DKH",
            },
            {
                "text": "2XL",
                "code": "2xl",
                "type": "SIZES",
                "sku_code": "AA05",
            }
        ]
    }
]


function main() {
    console.log('start')
    const uniqueSkuCode = new Map()
    metaValueProductStdVariant.forEach(({ meta_values }) => {
        for (const { sku_code, type } of meta_values) {
            (!uniqueSkuCode.has(sku_code)) && (uniqueSkuCode.set(sku_code, type))
        }
    })
    const a = uniqueSkuCode.values()
a.
    console.log(a)
}

main()