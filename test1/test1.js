
function main () {
    let countType=0
    let countColor=0
    let countSize=0
    const update = {
        $set: {
            count_attributes: {
                ...(countType > 0 ? { TYPES: countType } : {}),
                ...(countColor > 0 ? { COLORS: countColor } : {}),
                ...(countSize > 0 ? { SIZES: countSize } : {})
            }
        }
    }
    console.log(update)
}

main()